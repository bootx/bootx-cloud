# Bootx-Cloud
<p>
 <img src='https://gitee.com/bootx/bootx-cloud/badge/star.svg?theme=dark' alt='star'/>
 <img src="https://img.shields.io/badge/Java-17+-success.svg" alt="Build Status">
 <img src="https://img.shields.io/badge/Author-Bootx-orange.svg" alt="Build Status">
 <img src="https://img.shields.io/badge/Spring%20Boot-3.1-blue.svg" alt="Downloads">
 <img src="https://img.shields.io/badge/Spring%20Cloud-2022-blue.svg" alt="Coverage Status">
 <img src="https://img.shields.io/badge/license-Apache%20License%202.0-green.svg"/>
</p>

## 🍈项目介绍

​		Spring Cloud分布式敏捷开发系统架构，提供整套公共微服务服务模块，努力为打造全方位企业级开发解决方案，
致力将开源版打造成超越商业版后台管理框架的项目，**对应单体版实现 [Bootx-Platform](https://gitee.com/bootx/bootx-platform)**

##### 基础模块
- `bootx-cloud-core`: [gitee]()、[github]()
- `bootx-cloud-parent`: [gitee]()、[github]()
- `bootx-cloud-commons`: [gitee]()、[github]()
- `bootx-cloud-starter`: [gitee]()、[github]()
- `bootx-cloud-core`: [gitee]()、[github]()
##### 基础服务
- 平台管理服务
- iam服务
- 智能风控
- 网关鉴权

## 🥥项目体验

- 系统管理平台：[管理平台](http://web.cloud.bootx.cn/)
- Swagger聚合接口：[API文档](http://gateway.dev.bootx.cn:9000/doc.html)
- 日志分析：[Kibana管理平台](http://elk.dev.bootx.cn:5601/app/discover#)

## 🍒文档

- 前端项目地址：[https://gitee.com/bootx/bootx-cloud-ui](https://gitee.com/bootx/bootx-cloud-ui)
- 项目文档：[开发文档]()
- 项目启动：[启动文档]()
- 开发计划：[开发计划]()


## 🥞 系统架构

> 技术选型

- 编程语言：Java8+、Groovy、JavaScript
- 核心框架：Spring Boot、Spring Cloud、Spring Cloud Alibaba
- 持久层框架：MyBatis Plus
- 消息中间件：RabbitMQ
- 日志管理：Logstash-logback、Filebeat、ElasticSearch、Kibana
- 分布式中间件：Nacos、Sentinel、Sleuth、Zipkin、Seata

> 核心依赖

| 依赖                       | 版本     | 描述                          |
| -------------------------- | -------- | ----------------------------- |
|Spring Boot                | 2.5.4    |                               |
| Spring Cloud               | 2020.0.3 |                               |
| Spring Cloud Alibaba       | 2021.1   |                               |
| Mybatis Plus               | 3.4.3   | 持久层框架                 |
| Nacos                      | 2.0.2    | 替换为Nacos2.X                |
| Seata                      | 1.4.1    | 分布式事务                    |
| kryo                       | 0.41     | 序列化框架，seata在使用       |
| transmittable-thread-local | 2.12.1   |                               |


>系统架构图
![系统架构图](https://images.gitee.com/uploads/images/2021/0707/230002_0ab2d9b1_524686.png "系统架构图.png")


## 🍇项目特点

- 统一网关，实现鉴权、限流、黑白名单、访问记录等功能

- 分布式项目，前后端分离架构，方便二次开发

- 适用ELK进行日志管理，方便进行日志追踪


##  🥪 关于我们

微信扫码加入交流群，或添加微信号：`xxxx` 邀请进群


钉钉扫码加入钉钉交流群


QQ扫码加入QQ交流群
<p>
<img src="https://images.gitee.com/uploads/images/2021/0706/221552_934eb540_524686.png" width = "330" height = "500"/>
</p>

## 🍻 鸣谢
感谢 JetBrains 提供的免费开源 License：

[![JetBrains](https://oscimg.oschina.net/oscnet/up-4aab9fa8bc769295b48c888d93e71320d93.png)](https://www.jetbrains.com/?from=bootx)

感谢其他提供灵感和思路的开源项目

[部分参考的开源项目和开源许可列表](./_license/LICENSE.md)

##  🍷License

Apache License Version 2.0

